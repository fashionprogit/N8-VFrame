/**
 * Created by M on 2017/11/3.
 */
import "script-loader!static/vendors/jquery.js"
import 'script-loader!static/vendors/lodash.js'
import 'script-loader!static/vendors/moment.js'
import "script-loader!static/vendors/moment-timezone.js"
import "script-loader!static/vendors/jquery.signalR.js"
import "script-loader!static/vendors/js.cookie.js"
import "script-loader!static/vendors/Abp/Framework/scripts/abp.js"
import "script-loader!static/vendors/Abp/Framework/scripts/libs/abp.jquery.js"
import "script-loader!static/vendors/Abp/Framework/scripts/libs/abp.moment.js"
import 'font-awesome/css/font-awesome.min.css'
