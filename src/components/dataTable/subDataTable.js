/**
 * Created by M on 2017/11/17.
 */

const PAGE_SIZE = 10;
const PAGE_SIZE_ARRAY = [10, 50, 100, 150];
var subDataTable = {
  props: {
    cols: {
      type: Array,
      default: function() {
        return [];
      }
    },
    actions: {
      type: Array,
      default: function() {
        return [];
      }
    },
    selectType: {
      type: String //['single','multiple','action','']
    },
    singleSelectIcon: {
      type: String,
      default: "fa fa-search"
    },
    handleSelect: {
      type: Function,
      default: function() {
        return function(select) {};
      }
    },
    border: {
      type: Boolean,
      default: false
    },
    showHeader: {
      type: Boolean,
      default: true
    },
    stripe: {
      type: Boolean,
      default: false
    }
  },
  methods: {
    handleSelectionChange(val) {
      this.handleSelect(val);
    },
    handleSortChange(obj) {
      this.$emit("sort-change", obj);
    },
    handleAction({ commandName, row }) {
      this.$emit("action-click", { commandName, row });
    }
  },
  computed: {
    elementCols() {
      return this.cols.map(c => {
        if (!c.minWidth) {
          c.showOverflowTooltip = true;
        }
        return c;
      });
    }
  }
};

var subAbstractServerDataTable = {
  mixins: [subDataTable],
  props: {
    api: {
      type: String,
      default: ""
    },
    filterDto: {
      type: Object,
      default: function() {
        return {};
      }
    },
    beforeFetch: {
      type: Function
    },
    afterFetch: {
      type: Function
    },
    autoLoad: {
      type: Boolean,
      default: true
    }
  },
  data() {
    return {
      dataSource: [],
      loading: false,
      baseFilter: {
        sorting: ""
      }
    };
  },
  methods: {
    _fetch() {
      if (this.beforeFetch) {
        this.beforeFetch(filterDto);
      }
      this.loading = true;
      this.$v.http
        .request(this.api, Object.assign(this.filterDto, this.baseFilter))
        .then(result => {
          if (this.afterFetch) {
            this.afterFetch(result);
          }
          this.loading = false;
          this.__AbstractFetchLogic(result);
        });
    },
    __AbstractFetchLogic(result) {},
    //----Table Events-------------//
    handleSort({ column, prop, order }) {
      let order_direct =
        order == "ascending" ? " ASC" : order == "descending" ? " DESC" : "";
      this.baseFilter.sorting = (prop || "") + order_direct;
      this._fetch();
    },
    //----Table Events End-------------//
    refresh() {
      this.dataSource = [];
      this.pageIndex = 1;
      this.baseFilter.skipCount = (this.pageIndex - 1) * this.pageSize;
      this._fetch();
    },
    //刷新当前页
    refreshCurrent() {
      this.dataSource = [];
      this._fetch();
    }
  },
  created() {
    if (this.autoLoad) {
      this._fetch();
    }
  }
};

var subServerDataTableNoPaging = {
  mixins: [subAbstractServerDataTable],
  methods: {
    __AbstractFetchLogic(result) {
      this.dataSource = result.items;
    }
  }
};

var subServerDataTablePaging = {
  mixins: [subAbstractServerDataTable],
  data() {
    return {
      baseFilter: {
        sorting: "",
        maxResultCount: PAGE_SIZE,
        skipCount: 0
      },
      pageSize: PAGE_SIZE,
      pageSizeArray: PAGE_SIZE_ARRAY,
      pageIndex: 1,
      totalCount: 0,
      customSort: true
    };
  },
  methods: {
    __AbstractFetchLogic(result) {
      this.dataSource = result.items;
      this.totalCount = result.totalCount;
    },
    //----Table Events-------------//
    handleCurrentChange(currentPage) {
      this.pageIndex = currentPage;
      this.baseFilter.maxResultCount = this.pageSize;
      this.baseFilter.skipCount = (this.pageIndex - 1) * this.pageSize;
      this._fetch();
    },
    handleSizeChange(size) {
      this.pageSize = size;
      this.baseFilter.maxResultCount = this.pageSize;
      this.baseFilter.skipCount = (this.pageIndex - 1) * this.pageSize;
      this._fetch();
    }
    //----Table Events End-------------//
  }
};

export { subDataTable, subServerDataTableNoPaging, subServerDataTablePaging };
