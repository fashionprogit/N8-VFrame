export default {
  name: "nLookupSub",
  // props: {
  //   displayPath: {
  //     type: String
  //   },
  //   valuePath: {
  //     type: String
  //   },
  // },
  props: {
    value: {},
    text: {},
    selectItem: {},
    showType: {
      type: String,
      default: "popover"
    },
    clearable: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      visible: false,
      currentText: this.text
    }
  },
  methods: {
    set(text, value, selectItem) {
      this.currentText = text;
      this.$emit('input', value);
      this.$emit('update:selectItem', selectItem);
      this.$emit('selectChange');
    },
    closePop() {
      this.visible = false;
    },
    clear() {
      this.currentText = "";
      this.$emit('input', '');
      this.$emit('update:selectItem', {});
    }
  },
  watch: {
    text(v) {
      this.currentText = v;
    }
  }
}
