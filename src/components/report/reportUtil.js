/**
 * Created by M on 2018/10/24.
 */
const reportUrl = "/static/stimulsoft/stimulsoft.reports.js";
const viewerUrl = "/static/stimulsoft/stimulsoft.viewer.js";
const designerUrl = "/static/stimulsoft/stimulsoft.designer.js";
class ReportUtil {
  _loadJs(url) {
    return $.ajax({
      url: url,
      dataType: "script",
      cache: true
    });
  }

  _loadReportJs() {
    return new Promise((resolve, reject) => {
      if (window.Stimulsoft) {
        resolve();
      } else {
        this._loadJs(reportUrl).then(_ => {
          Stimulsoft.Base.StiLicense.key = "6vJhGtLLLz2GNviWmUTrhSqnOItdDwjBylQzQcAOiHkQlDLrf+mQ3m6yA9yftOcJYksMlOXigZoUPBeiPOShEo7rc1" +
            "J48+e8ItQ6k5KODDOCS5H0112SoKol3eDFGEjjBM7/ZSvAblZRv1KL8t1w3nsH+a/Zawn6ZsMPprLCBx1OLQGBDrSh" +
            "Tt07fkeSlvW5xD2dr/CiuvAcewAOStpyOdRCji866lyWfOW/c1Qgt+iZBr/dZP9AYK+RcNW4IcIt5xRQXvkYS9yLDE" +
            "LHq47TVhd9s5Ac7bObO1Wy6mUBxKgYt3sNDAjAcyRdJA00jauOw1GMe41cuUfd5q4VjNslXybxncOCHtS9Pxm8rOtO" +
            "T5NSebK4p7RPYGqYVxBIOz5Scfghtc6Z4KJk34dpegVSTfrKaq2ZQnwL1hfoQHNu0FWtKSNX3fO4+PzL9j3zwt3yWY" +
            "ey4jRnqtwI7Zo0/BzemGHHanbDONwssgYnLq8xC9j/BcEx5X158NVMpwlZIVX0PfUG+aBMLpzpJQoQ0adHwf/KV/9z" +
            "YG+/FMHGEERADcLrnh9OS/EB1a0TEHsvz4tl";
          resolve();
        }).catch(reject);
      }
    })
  }

  _loadViewerJs() {
    return new Promise((resolve, reject) => {
      this._loadReportJs().then(_ => {
        if (window.Stimulsoft.Viewer && window.Stimulsoft.Viewer.StiViewer) {
          resolve();
        } else {
          this._loadJs(viewerUrl).then(resolve).catch(reject);
        }
      }).catch(reject);
    });
  }

  _loadDesignerJs() {
    return new Promise((resolve, reject) => {
      this._loadViewerJs().then(_ => {
        if (window.Stimulsoft.Designer && window.Stimulsoft.Designer.StiDesigner) {
          resolve();
        } else {
          this._loadJs(designerUrl).then(resolve).catch(reject);
        }
      }).catch(reject);
    });
  }

  designerReady() {
    return this._loadDesignerJs();
  }

  viewerReady() {
    return this._loadViewerJs();
  }

  /*
   @report:报表json字符串
   @data:数据json对象
   */
  _getReport(report, data) {
    var stiReport = new Stimulsoft.Report.StiReport();

    if (report) {
      stiReport.load(report);
    }

    var dataSet = new Stimulsoft.System.Data.DataSet("ReportDataSet");
    dataSet.readJson(data);
    stiReport.regData(dataSet.dataSetName, "", dataSet);
    stiReport.dictionary.synchronize();
    return stiReport;
  }

  /*
   @{stiReport,report,data}

   @@stiReport:报表对象， 可以通过reportViewer或reportDesigner获取
   互斥
   @@report:报表json字符串
   @@data:数据json对象
   */
  print({stiReport, report, data}) {
    var vm = this;
    return vm.viewerReady().then(_ => {
      if (stiReport) {
        stiReport.print();
      } else {
        var sReport = vm._getReport(report, data);
        sReport.render();
        sReport.print();
      }
    });
  }
}

export default new ReportUtil()
