var report = JSON.stringify({
  "ReportVersion": "2018.1.8",
  "ReportGuid": "9e51acfdef36ce6f00030eed5871d30b",
  "ReportName": "Report",
  "ReportAlias": "Report",
  "ReportCreated": "/Date(1540463227000+0800)/",
  "ReportChanged": "/Date(1540463227000+0800)/",
  "EngineVersion": "EngineV2",
  "CalculationMode": "Interpretation",
  "ReportUnit": "Centimeters",
  "Dictionary": {
    "DataSources": {
      "0": {
        "Ident": "StiDataTableSource",
        "Name": "shop",
        "Alias": "shop",
        "Columns": {
          "0": {
            "Name": "code",
            "Index": -1,
            "NameInSource": "code",
            "Alias": "code",
            "Type": "System.String"
          }, "1": {"Name": "name", "Index": -1, "NameInSource": "name", "Alias": "name", "Type": "System.String"}
        },
        "NameInSource": "ReportDataSet.shop"
      },
      "1": {
        "Ident": "StiDataTableSource",
        "Name": "orderItems",
        "Alias": "orderItems",
        "Columns": {
          "0": {
            "Name": "tmallOrderId",
            "Index": -1,
            "NameInSource": "tmallOrderId",
            "Alias": "tmallOrderId",
            "Type": "System.String"
          },
          "1": {
            "Name": "bilType",
            "Index": -1,
            "NameInSource": "bilType",
            "Alias": "bilType",
            "Type": "System.Decimal"
          },
          "2": {
            "Name": "bilTypeText",
            "Index": -1,
            "NameInSource": "bilTypeText",
            "Alias": "bilTypeText",
            "Type": "System.String"
          },
          "3": {"Name": "bilCode", "Index": -1, "NameInSource": "bilCode", "Alias": "bilCode", "Type": "System.String"},
          "4": {"Name": "payTm", "Index": -1, "NameInSource": "payTm", "Alias": "payTm", "Type": "System.String"},
          "5": {
            "Name": "tradeStus",
            "Index": -1,
            "NameInSource": "tradeStus",
            "Alias": "tradeStus",
            "Type": "System.String"
          },
          "6": {
            "Name": "orderProcessStus",
            "Index": -1,
            "NameInSource": "orderProcessStus",
            "Alias": "orderProcessStus",
            "Type": "System.Decimal"
          },
          "7": {
            "Name": "orderProcessStusText",
            "Index": -1,
            "NameInSource": "orderProcessStusText",
            "Alias": "orderProcessStusText",
            "Type": "System.String"
          },
          "8": {
            "Name": "failReason",
            "Index": -1,
            "NameInSource": "failReason",
            "Alias": "failReason",
            "Type": "System.String"
          },
          "9": {
            "Name": "buyerRemark",
            "Index": -1,
            "NameInSource": "buyerRemark",
            "Alias": "buyerRemark",
            "Type": "System.String"
          },
          "10": {
            "Name": "orderSource",
            "Index": -1,
            "NameInSource": "orderSource",
            "Alias": "orderSource",
            "Type": "System.String"
          },
          "11": {"Name": "id", "Index": -1, "NameInSource": "id", "Alias": "id", "Type": "System.Decimal"}
        },
        "NameInSource": "ReportDataSet.orderItems"
      }
    }
  },
  "Pages": {
    "0": {
      "Ident": "StiPage",
      "Name": "Page1",
      "Guid": "853b8fdfe099d562d8d84de6f8bed55a",
      "Interaction": {"Ident": "StiInteraction"},
      "Border": ";;2;;;;;solid:Black",
      "Brush": "solid:Transparent",
      "Components": {
        "0": {
          "Ident": "StiReportTitleBand",
          "Name": "ReportTitleBand1",
          "ClientRectangle": "0,0.4,19,2.2",
          "Interaction": {"Ident": "StiInteraction"},
          "Border": ";;;;;;;solid:Black",
          "Brush": "solid:Transparent",
          "Components": {
            "0": {
              "Ident": "StiText",
              "Name": "Text1",
              "ClientRectangle": "3.6,0.8,3,0.6",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{shop.code}"},
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black"
            },
            "1": {
              "Ident": "StiText",
              "Name": "Text2",
              "ClientRectangle": "10,0.8,3,0.6",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{shop.name}"},
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black"
            }
          }
        },
        "1": {
          "Ident": "StiDataBand",
          "Name": "DataorderItems",
          "ClientRectangle": "0,3.4,19,0.8",
          "Interaction": {"Ident": "StiBandInteraction"},
          "Border": ";;;;;;;solid:Black",
          "Brush": "solid:Transparent",
          "Components": {
            "0": {
              "Ident": "StiText",
              "Name": "DataorderItems_tmallOrderId",
              "CanGrow": true,
              "ClientRectangle": "0,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.tmallOrderId}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "1": {
              "Ident": "StiText",
              "Name": "DataorderItems_bilType",
              "CanGrow": true,
              "ClientRectangle": "1.6,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.bilType}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "2": {
              "Ident": "StiText",
              "Name": "DataorderItems_bilTypeText",
              "CanGrow": true,
              "ClientRectangle": "3.2,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.bilTypeText}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "3": {
              "Ident": "StiText",
              "Name": "DataorderItems_bilCode",
              "CanGrow": true,
              "ClientRectangle": "4.8,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.bilCode}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "4": {
              "Ident": "StiText",
              "Name": "DataorderItems_payTm",
              "CanGrow": true,
              "ClientRectangle": "6.4,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.payTm}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "5": {
              "Ident": "StiText",
              "Name": "DataorderItems_tradeStus",
              "CanGrow": true,
              "ClientRectangle": "8,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.tradeStus}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "6": {
              "Ident": "StiText",
              "Name": "DataorderItems_orderProcessStus",
              "CanGrow": true,
              "ClientRectangle": "9.6,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.orderProcessStus}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "7": {
              "Ident": "StiText",
              "Name": "DataorderItems_orderProcessStusText",
              "CanGrow": true,
              "ClientRectangle": "11.2,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.orderProcessStusText}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "8": {
              "Ident": "StiText",
              "Name": "DataorderItems_failReason",
              "CanGrow": true,
              "ClientRectangle": "12.8,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.failReason}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "9": {
              "Ident": "StiText",
              "Name": "DataorderItems_buyerRemark",
              "CanGrow": true,
              "ClientRectangle": "14.4,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.buyerRemark}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "10": {
              "Ident": "StiText",
              "Name": "DataorderItems_orderSource",
              "CanGrow": true,
              "ClientRectangle": "16,0,1.6,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.orderSource}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            },
            "11": {
              "Ident": "StiText",
              "Name": "DataorderItems_id",
              "CanGrow": true,
              "ClientRectangle": "17.6,0,1.4,0.8",
              "Interaction": {"Ident": "StiInteraction"},
              "Text": {"Value": "{orderItems.id}"},
              "VertAlignment": "Center",
              "Border": ";;;;;;;solid:Black",
              "Brush": "solid:Transparent",
              "TextBrush": "solid:Black",
              "TextOptions": {"WordWrap": true}
            }
          },
          "DataSourceName": "orderItems"
        }
      },
      "PageWidth": 21,
      "PageHeight": 29.7,
      "Watermark": {"TextBrush": "solid:50,0,0,0"},
      "Margins": {"Left": 1, "Right": 1, "Top": 1, "Bottom": 1}
    }
  }
});

var data = {
  shop: {code: "001", name: "xx店铺"},
  orderItems: [{
    "tmallOrderId": "E20181021211717067700009",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181021211717067700009",
    "payTm": "2018-10-21T21:17:42+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21565
  }, {
    "tmallOrderId": "E20181021202608096800007",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181021202608096800007",
    "payTm": "2018-10-21T20:26:26+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21566
  }, {
    "tmallOrderId": "E20181021201214082000006",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181021201214082000006",
    "payTm": "2018-10-21T20:12:21+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21567
  }, {
    "tmallOrderId": "E20181019124910091500009",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181019124910091500009",
    "payTm": "2018-10-19T12:49:34+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21568
  }, {
    "tmallOrderId": "E20181019124841091500011",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181019124841091500011",
    "payTm": "2018-10-19T12:48:54+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21569
  }, {
    "tmallOrderId": "E20181019114751030900013",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181019114751030900013",
    "payTm": "2018-10-19T11:48:04+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21570
  }, {
    "tmallOrderId": "E20181018194637071000007",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181018194637071000007",
    "payTm": "2018-10-18T19:46:53+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21571
  }, {
    "tmallOrderId": "E20181018181639095100004",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181018181639095100004",
    "payTm": "2018-10-18T18:16:55+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21573
  }, {
    "tmallOrderId": "E20181018152847101700023",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181018152847101700023",
    "payTm": "2018-10-18T15:29:18+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21574
  }, {
    "tmallOrderId": "E20181018120203086500010",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181018120203086500010",
    "payTm": "2018-10-18T12:02:15+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21576
  }, {
    "tmallOrderId": "E20181017175610009600013",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181017175610009600013",
    "payTm": "2018-10-17T17:56:18+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21577
  }, {
    "tmallOrderId": "E20181017113537009600011",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181017113537009600011",
    "payTm": "2018-10-17T11:35:46+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21578
  }, {
    "tmallOrderId": "E20181016180302036900008",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181016180302036900008",
    "payTm": "2018-10-16T18:03:18+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21580
  }, {
    "tmallOrderId": "E20181015213939001000012",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181015213939001000012",
    "payTm": "2018-10-15T21:39:59+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21582
  }, {
    "tmallOrderId": "E20181015184915082000005",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181015184915082000005",
    "payTm": "2018-10-15T18:49:22+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21583
  }, {
    "tmallOrderId": "E20181015174921093100014",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181015174921093100014",
    "payTm": "2018-10-15T17:49:31+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21584
  }, {
    "tmallOrderId": "E20180821230710009600007",
    "bilType": 1,
    "bilTypeText": "销售订单",
    "bilCode": "SO-E20180821230710009600007",
    "payTm": "2018-08-21T23:07:18+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21547
  }, {
    "tmallOrderId": "E20180821100823069100002",
    "bilType": 1,
    "bilTypeText": "销售订单",
    "bilCode": "SO-E20180821100823069100002",
    "payTm": "2018-08-21T10:08:45+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21548
  }, {
    "tmallOrderId": "E20180821095016080700006",
    "bilType": 1,
    "bilTypeText": "销售订单",
    "bilCode": "SO-E20180821095016080700006",
    "payTm": "2018-08-21T09:50:48+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21549
  }, {
    "tmallOrderId": "E20180821094931020900006",
    "bilType": 1,
    "bilTypeText": "销售订单",
    "bilCode": "SO-E20180821094931020900006",
    "payTm": "2018-08-21T09:49:54+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21550
  }, {
    "tmallOrderId": "E20181021211717067700009",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181021211717067700009",
    "payTm": "2018-10-21T21:17:42+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21565
  }, {
    "tmallOrderId": "E20181021202608096800007",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181021202608096800007",
    "payTm": "2018-10-21T20:26:26+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21566
  }, {
    "tmallOrderId": "E20181021201214082000006",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181021201214082000006",
    "payTm": "2018-10-21T20:12:21+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21567
  }, {
    "tmallOrderId": "E20181019124910091500009",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181019124910091500009",
    "payTm": "2018-10-19T12:49:34+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21568
  }, {
    "tmallOrderId": "E20181019124841091500011",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181019124841091500011",
    "payTm": "2018-10-19T12:48:54+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21569
  }, {
    "tmallOrderId": "E20181019114751030900013",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181019114751030900013",
    "payTm": "2018-10-19T11:48:04+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21570
  }, {
    "tmallOrderId": "E20181018194637071000007",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181018194637071000007",
    "payTm": "2018-10-18T19:46:53+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21571
  }, {
    "tmallOrderId": "E20181018181639095100004",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181018181639095100004",
    "payTm": "2018-10-18T18:16:55+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21573
  }, {
    "tmallOrderId": "E20181018152847101700023",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181018152847101700023",
    "payTm": "2018-10-18T15:29:18+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21574
  }, {
    "tmallOrderId": "E20181018120203086500010",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181018120203086500010",
    "payTm": "2018-10-18T12:02:15+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21576
  }, {
    "tmallOrderId": "E20181017175610009600013",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181017175610009600013",
    "payTm": "2018-10-17T17:56:18+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21577
  }, {
    "tmallOrderId": "E20181017113537009600011",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181017113537009600011",
    "payTm": "2018-10-17T11:35:46+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21578
  }, {
    "tmallOrderId": "E20181016180302036900008",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181016180302036900008",
    "payTm": "2018-10-16T18:03:18+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21580
  }, {
    "tmallOrderId": "E20181015213939001000012",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181015213939001000012",
    "payTm": "2018-10-15T21:39:59+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21582
  }, {
    "tmallOrderId": "E20181015184915082000005",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181015184915082000005",
    "payTm": "2018-10-15T18:49:22+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21583
  }, {
    "tmallOrderId": "E20181015174921093100014",
    "bilType": 0,
    "bilTypeText": "[0]",
    "bilCode": "SO-E20181015174921093100014",
    "payTm": "2018-10-15T17:49:31+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21584
  }, {
    "tmallOrderId": "E20180821230710009600007",
    "bilType": 1,
    "bilTypeText": "销售订单",
    "bilCode": "SO-E20180821230710009600007",
    "payTm": "2018-08-21T23:07:18+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21547
  }, {
    "tmallOrderId": "E20180821100823069100002",
    "bilType": 1,
    "bilTypeText": "销售订单",
    "bilCode": "SO-E20180821100823069100002",
    "payTm": "2018-08-21T10:08:45+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21548
  }, {
    "tmallOrderId": "E20180821095016080700006",
    "bilType": 1,
    "bilTypeText": "销售订单",
    "bilCode": "SO-E20180821095016080700006",
    "payTm": "2018-08-21T09:50:48+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21549
  }, {
    "tmallOrderId": "E20180821094931020900006",
    "bilType": 1,
    "bilTypeText": "销售订单",
    "bilCode": "SO-E20180821094931020900006",
    "payTm": "2018-08-21T09:49:54+08:00",
    "tradeStus": "TRADE_SUCCESS",
    "orderProcessStus": 0,
    "orderProcessStusText": "等待付款",
    "failReason": null,
    "buyerRemark": "",
    "orderSource": "线上(yz)",
    "id": 21550
  }]
};


export {report, data}
