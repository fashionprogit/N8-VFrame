// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router/index.js'
//依赖
import './vendors'
import {VFrame, SessionService} from './output.js'
Vue.config.productionTip = false

/* eslint-disable no-new */

VFrame.start().then(() => {
  var session = new SessionService(router);

  Vue.prototype.$session = session;
  
  session.init().then(result => {
    new Vue({
      el: '#app',
      router,
      template: '<App/>',
      components: {App}
    })
  });
})

