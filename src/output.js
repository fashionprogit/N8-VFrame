import vue from 'vue'
import VueQuillEditor from 'vue-quill-editor'
import elementUI from 'element-ui'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
import 'quill/dist/quill.core.css'
import '../theme/index.css'
import './assets/site.css'
import VFrame from './core/core.js'
import {
  UrlHelper,
  AuthenticateHelper,
  ClockHelper,
  HttpHelper,
  routerGuard
} from './core/core.help.js'
import {
  SessionService,
  LoginService
} from './core/core.services.js'
import './core/abp.implemented'
import reportUtil from './components/report/reportUtil.js'
/*
 组件
 */
import nUpload from './components/upload/nUpload.vue'
import nMutipleUpload from './components/upload/nMutipleUpload.vue'
import nToolbar from './components/toolbar/nToolbar.vue'
import nLayout from './components/layout/nLayout.vue'
import nListLayout from './components/layout/nListLayout.vue'
import nDialogLayout from './components/dialog/nDialogLayout.vue'
import nLookup from './components/lookup/nLookup.vue'
import nDataTableNoPaging from './components/dataTable/nDataTableNoPaging.vue'
import nDataTablePaging from './components/dataTable/nDataTablePaging.vue'
import nServerDataTableNoPaging from './components/dataTable/nServerDataTableNoPaging.vue'
import nServerDataTablePaging from './components/dataTable/nServerDataTablePaging.vue'
import nDatepicker from './components/datePicker/nDatepicker.vue'
import chatBar from './components/chat/chatBar.vue'
import advanceFilterPanel from './components/advanceFilterPanel/advanceFilterPanel.vue'
import nEditor from './components/editor/nEditor.vue'
import nQr from './components/qrcode/nQr.vue'
import nSearchInput from './components/searchInput/nSearchInput.vue'
import reportDesigner from './components/report/reportDesigner.vue'
import reportViewer from './components/report/reportViewer.vue'
import keyboard from './components/keyboard/keyBoard.vue'

import nVueGrid from './components/layout/vue-js-grid/grid.vue'


vue.use(elementUI);
vue.use(VueQuillEditor, {
  theme: 'snow'
})

//组件全部使用
vue.component(nUpload.name, nUpload);
vue.component(nMutipleUpload.name, nMutipleUpload);
vue.component(nToolbar.name, nToolbar);
vue.component(nLayout.name, nLayout);
vue.component(nListLayout.name, nListLayout);
vue.component(nDataTableNoPaging.name, nDataTableNoPaging);
vue.component(nDataTablePaging.name, nDataTablePaging);
vue.component(nServerDataTableNoPaging.name, nServerDataTableNoPaging);
vue.component(nServerDataTablePaging.name, nServerDataTablePaging);
vue.component(nDatepicker.name, nDatepicker);
vue.component(nDialogLayout.name, nDialogLayout);
vue.component(nLookup.name, nLookup);
vue.component(chatBar.name, chatBar);
vue.component(advanceFilterPanel.name, advanceFilterPanel);
vue.component(nEditor.name, nEditor);
vue.component(nQr.name, nQr);
vue.component(nSearchInput.name, nSearchInput);
vue.component(reportDesigner.name, reportDesigner);
vue.component(reportViewer.name, reportViewer);
vue.component(keyboard.name,keyboard);

//vue-js-grid
vue.component(nVueGrid.name, nVueGrid);

vue.prototype.$v = VFrame;
vue.prototype.l = VFrame.l;
vue.prototype.$abp = abp;
vue.prototype.$report = reportUtil

//dialog方法
vue.prototype.$dialog = {
  open(component, args) {
    return new Promise((resolve) => {

      let Dialog = vue.extend(component)

      var $vm = new Dialog({
        el: document.createElement('div')
      })

      var node = document.body.appendChild($vm.$el)

      $vm.open(args).then(result => {
        if (resolve) {
          resolve(result);
        }
        node.remove();
        $vm.$destroy();
      }, () => {
        // try {
        //   if (reject) {
        //     reject();
        //   }
        // } catch (err) {
        // }
        node.remove();
        $vm.$destroy();
      });
    });
  }
}

vue.prototype.seq = {
  seq: 1000,
  getMax() {
    this.seq = this.seq + 1;
    return this.seq;
  }
}

export {
  VFrame,
  UrlHelper,
  AuthenticateHelper,
  ClockHelper,
  HttpHelper,
  SessionService,
  LoginService,
  routerGuard
}
