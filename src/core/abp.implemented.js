/**
 * Created by M on 2017/11/10.
 */
import { MessageBox, Notification, Message } from "element-ui";
import VFrame from "./core";
const notify = {},
  message = {};

notify.success = function(message, title, options) {
  Notification.success(
    $.extend(
      {
        title: title || VFrame.l("Success"),
        message: message
      },
      options
    )
  );
};

notify.info = function(message, title, options) {
  Notification.info(
    $.extend(
      {
        title: title || VFrame.l("Alert"),
        message: message
      },
      options
    )
  );
};

notify.warn = function(message, title, options) {
  Notification(
    $.extend(
      {
        title: title || VFrame.l("Warning"),
        message: message,
        type: "warning"
      },
      options
    )
  );
};

notify.error = function(message, title, options) {
  Notification.error(
    $.extend(
      {
        title: title || VFrame.l("Error"),
        message: message
      },
      options
    )
  );
};

message.info = function(message, title) {
  return MessageBox.alert(message, title || VFrame.l("Alert"), {
    confirmButtonText: VFrame.l("QUEDING"),
    type: "info"
  });
};

message.success = function(message, title) {
  return MessageBox.alert(message, title || VFrame.l("Success"), {
    confirmButtonText: VFrame.l("QUEDING"),
    type: "success"
  });
};

message.warn = function(message, title) {
  return MessageBox.alert(message, title || VFrame.l("Warning"), {
    confirmButtonText: VFrame.l("QUEDING"),
    type: "warning"
  });
};

message.error = function(message, title) {
  return MessageBox.alert(message, title || VFrame.l("Error"), {
    confirmButtonText: VFrame.l("QUEDING"),
    type: "error"
  });
};

message.confirm = function(message, title) {
  return MessageBox.confirm(message, title || VFrame.l("QUEDING"), {
    confirmButtonText: VFrame.l("QUEDING"),
    cancelButtonText: VFrame.l("QUXIAO")
  });
};

abp.notify = notify;
abp.message = message;
abp.utils = abp.utils || {};
abp.utils = Object.assign(
  {
    title: function(title) {
      title = title || "FashionPro";
      window.document.title = title;
    }
  },
  abp.utils
);
