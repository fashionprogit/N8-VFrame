/**
 * Created by M on 2017/11/14.
 */
import VFrame from "../core";
import { UrlHelper } from "../core.help";

export default class LoginService {
  constructor(router) {
    this.$router = router;
  }

  authenticate(authenticateModel, callback, redirectUrl) {
    authenticateModel.twoFactorRememberClientToken = abp.utils.getCookieValue(
      VFrame.twoFactorRememberClientTokenName
    );
    authenticateModel.singleSignIn = UrlHelper.getSingleSignIn();
    authenticateModel.returnUrl = UrlHelper.getReturnUrl();

    VFrame.http
      .request("/api/TokenAuth/Authenticate", authenticateModel)
      .then(result => {
        if (callback) callback();
        this.processAuthenticateResult(result, redirectUrl);
      })
      .finally(callback);
  }
  processAuthenticateResult(authenticateResult, redirectUrl) {
    if (authenticateResult.shouldResetPassword) {
      this.$router.push({
        path: "/account/reset-password",
        query: {
          userId: authenticateResult.userId,
          tenantId: abp.session.tenantId,
          resetCode: authenticateResult.passwordResetCode
        }
      });
    } else if (authenticateResult.requiresTwoFactorVerification) {
      //TODO:send code router
    } else if (authenticateResult.accessToken) {
      if (authenticateResult.returnUrl && !redirectUrl) {
        redirectUrl = authenticateResult.returnUrl;
      }
      this.login(
        authenticateResult.accessToken,
        authenticateResult.encryptedAccessToken,
        authenticateResult.expireInSeconds,
        this.rememberMe,
        authenticateResult.twoFactorRememberClientToken,
        redirectUrl
      );
    } else {
      abp.log.warn("Unexpected authenticateResult!");
      this.$router.replace("/account/login");
    }
  }

  login(
    accessToken,
    encryptedAccessToken,
    expireInSeconds,
    rememberMe,
    twoFactorRememberClientToken,
    redirectUrl
  ) {
    let tokenExpireDate = rememberMe
      ? new Date(new Date().getTime() + 1000 * expireInSeconds)
      : undefined;

    abp.auth.setToken(accessToken, tokenExpireDate);

    abp.utils.setCookieValue(
      VFrame.authorization.encrptedAuthTokenName,
      encryptedAccessToken,
      tokenExpireDate,
      abp.appPath
    );

    if (twoFactorRememberClientToken) {
      abp.utils.setCookieValue(
        VFrame.twoFactorRememberClientTokenName,
        twoFactorRememberClientToken,
        new Date(new Date().getTime() + 365 * 86400000), // 1 year
        abp.appPath
      );
    }

    if (redirectUrl) {
      location.href = redirectUrl;
    } else {
      let initialUrl = UrlHelper.initialUrl;

      if (initialUrl.indexOf("/account") > 0) {
        initialUrl = VFrame.appBaseUrl;
      }
      // if (location.href == initialUrl) {
      //   location.reload(initialUrl);
      // } else {
      //   location.href = initialUrl;
      // }
      location.href = VFrame.appBaseUrl; //(initialUrl);
    }
  }
}
