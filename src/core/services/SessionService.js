import VFrame from '../core'
export default class SessionService {
  /*
   return Promise
   */
  init() {
    return new Promise((resolve, reject) => {
      VFrame.http.request('/api/services/app/Session/GetCurrentLoginInformations', {}).then(result => {
        this._application = result.application;
        this._user = result.user;
        this._tenant = result.tenant;
        resolve(true);
      }).catch(err => {
        reject(err);
      });
    })
  }

  get application() {
    return this._application;
  }

  get user() {
    return this._user;
  }

  get userId() {
    return this.user ? this.user.id : null;
  }

  get tenant() {
    return this._tenant;
  }

  get tenantId() {
    return this.tenant ? this.tenant.id : null;
  }

  getShownLoginName() {
    const userName = this._user.userName;
    if (!this._abpMultiTenancyService.isEnabled) {
      return userName;
    }

    return (this._tenant ? this._tenant.tenancyName : '.') + '\\' + userName;
  }

  changeTenantIfNeeded(tenantId) {
    if (this.isCurrentTenant(tenantId)) {
      return false;
    }

    abp.multiTenancy.setTenantIdCookie(tenantId);
    location.reload();
    return true;
  }

  isCurrentTenant(tenantId) {
    if (!tenantId && this.tenant) {
      return false;
    } else if (tenantId && (!this.tenant || this.tenant.id !== tenantId)) {
      return false;
    }

    return true;
  }

}
