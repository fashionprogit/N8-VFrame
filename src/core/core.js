import { UrlHelper, AuthenticateHelper, HttpHelper } from "./core.help.js";

export default class VFrame {
  static isDebug = true;
  static tenancyNamePlaceHolderInUrl = "{TENANCY_NAME}";
  static appBaseUrl;
  static appReportServerUrl;
  static appBaseUrlFormat;
  static remoteServiceBaseUrl;
  static remoteServiceBaseUrlFormat;
  static contentUrl;
  static recaptchaSiteKey;
  static subscriptionExpireNootifyDayCount;
  static remoteStaticApi = {
    getAll: "/AbpUserConfiguration/GetAll"
  };

  static authorization = {
    encrptedAuthTokenName: "enc_auth_token"
  };
  static localization = {
    defaultLocalizationSourceName: "AKProject"
  };
  static twoFactorRememberClientTokenName = "TwoFactorRememberClientToken";
  static http = new HttpHelper();
  static lookupSchemaDict = {};

  /**
   * 在vue实例化之前调用异步start方法
   */
  static start() {
    return new Promise((resolve, reject) => {
      abp
        .ajax({
          url: "static/appconfig.json",
          method: "get",
          headers: [
            {
              "Abp.TenantId": abp.multiTenancy.getTenantIdCookie()
            }
          ]
        })
        .done(result => {
          //INFO:域名分辨租户(无多租户，不需要)
          // const subdomainTenancyNameFinder = new SubdomainTenancyNameFinder();
          // const tenancyName = subdomainTenancyNameFinder.getCurrentTenancyNameOrNull(result.appBaseUrl);
          VFrame.isDebug = result.debug;
          VFrame.contentUrl = result.contentUrl;

          VFrame.appBaseUrlFormat = result.appBaseUrl;
          VFrame.remoteServiceBaseUrlFormat = result.remoteServiceBaseUrlFormat;
          VFrame.appBaseUrl = result.appBaseUrl.replace(
            VFrame.tenancyNamePlaceHolderInUrl + ".",
            ""
          );
          VFrame.remoteServiceBaseUrl = result.remoteServiceBaseUrl.replace(
            VFrame.tenancyNamePlaceHolderInUrl + ".",
            ""
          );
          VFrame.appReportServerUrl = result.appReportServerUrl;

          VFrame.authenticate(() => {
            resolve();
          });
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  static authenticate(callback) {
    const queryStringObj = UrlHelper.getQueryParameters();

    // 注册租户（弃用）
    // if (queryStringObj.redirect && queryStringObj.redirect === 'TenantRegistration') {
    //   if (queryStringObj.forceNewRegistration) {
    //     VFrame.logout();
    //   }
    //   location.href = AppConsts.appBaseUrl + '/account/select-edition';
    // }

    if (queryStringObj.impersonationToken) {
      AuthenticateHelper.impersonatedAuthenticate(
        queryStringObj.impersonationToken,
        queryStringObj.tenantId
      ).then(() => {
        AuthenticateHelper.getUserConfiguration(callback);
      });
    } else if (queryStringObj.switchAccountToken) {
      AuthenticateHelper.linkedAccountAuthenticate(
        queryStringObj.switchAccountToken,
        queryStringObj.tenantId,
        () => {
          AuthenticateHelper.getUserConfiguration(callback);
        }
      );
    } else {
      AuthenticateHelper.getUserConfiguration(callback);
    }
  }

  /**
   * 权限验证
   * @param {*权限名称} permissionName
   */
  static isGranted(permissionName) {
    return abp.auth.isGranted(permissionName);
  }

  /**
   * 多语言方法
   * @param {*多语言key} key
   * @param {*其他参数} args
   */
  static l(key, args) {
    let localizationSourceName =
      VFrame.localization.defaultLocalizationSourceName;

    let localizedText = abp.localization.localize(key, localizationSourceName);

    if (!localizedText) {
      abp.log.error("miss localize key:" + key);
      localizedText = key;
    }

    if (typeof args == "number") {
      args = args + "";
    }

    if (!args || !args.length) {
      return localizedText;
    }

    // args[0].unshift(localizedText);

    return abp.utils.formatString(localizedText, args);
  }

  /**
   * 注销
   * @param reload
   * @param returnUrl
   */
  static logout(reload, returnUrl) {
    abp.auth.clearToken();
    if (reload !== false) {
      if (returnUrl) {
        location.href = returnUrl;
      } else {
        location.href = VFrame.appBaseUrl;
      }
    }
  }

  static setEncryptedTokenCookie(encryptedToken) {
    abp.utils.setCookieValue(
      VFrame.authorization.encrptedAuthTokenName,
      encryptedToken,
      new Date(new Date().getTime() + 365 * 86400000), //1 year
      abp.appPath
    );
  }

  /**
   * 初始化signalr
   * @param callback
   */
  static initSignalR(callback) {
    jQuery.getScript(VFrame.remoteServiceBaseUrl + "/signalr/hubs", () => {
      $.connection.hub.url = VFrame.remoteServiceBaseUrl + "/signalr";

      const encryptedAuthToken = abp.utils.getCookieValue(
        VFrame.authorization.encrptedAuthTokenName
      );

      $.connection.hub.qs =
        VFrame.authorization.encrptedAuthTokenName +
        "=" +
        encodeURIComponent(encryptedAuthToken);

      jQuery.getScript(VFrame.appBaseUrl + "/static/abp/abp.signalr.js");

      callback();
    });
  }
}
