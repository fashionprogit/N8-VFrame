/**
 * Created by M on 2017/11/3.
 */
/**
 * 用于获取url的get参数
 */
export default class UrlHelper {
  /**
   * The URL requested, before initial routing.
   */
  static initialUrl = location.href;

  static getQueryParameters() {
    return this.getQueryParametersUsingParameters(document.location.search);
  }

  static getQueryParametersUsingParameters(search) {
    return search.replace(/(^\?)/, '').split('&').map(function (n) {
      return n = n.split('='), this[n[0]] = n[1], this;
    }.bind({}))[0];
  }

  static getInitialUrlParameters() {
    let questionMarkIndex = this.initialUrl.indexOf('?');
    if (questionMarkIndex >= 0) {
      return this.initialUrl.substr(questionMarkIndex, this.initialUrl.length - questionMarkIndex);
    }

    return '';
  }

  static getReturnUrl() {
    const queryStringObj = this.getQueryParametersUsingParameters(this.getInitialUrlParameters());
    if (queryStringObj.returnUrl) {
      return decodeURIComponent(queryStringObj.returnUrl);
    }

    return null;
  }

  static getSingleSignIn() {
    const queryStringObj = this.getQueryParametersUsingParameters(this.getInitialUrlParameters());
    if (queryStringObj.ss) {
      return queryStringObj.ss;
    }

    return false;
  }
}
