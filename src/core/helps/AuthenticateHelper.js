/**
 * Created by M on 2017/11/3.
 */
import vFrame from '../core.js'
import ClockHelper from './ClockHelper.js'
/**
 * 用户认证
 */
export default class AuthenticateHelper {
  static impersonatedAuthenticate(impersonationToken, tenantId, callback) {
    abp.multiTenancy.setTenantIdCookie(tenantId);
    const cookieLangValue = abp.utils.getCookieValue('Abp.Localization.CultureName');

    abp.ajax({
      url: vFrame.remoteServiceBaseUrl + '/api/TokenAuth/ImpersonatedAuthenticate?impersonationToken=' + impersonationToken,
      method: 'POST',
      headers: {
        '.AspNetCore.Culture': ('c=' + cookieLangValue + '|uic=' + cookieLangValue),
        'Abp.TenantId': abp.multiTenancy.getTenantIdCookie()
      }
    }).done(result => {
      abp.auth.setToken(result.accessToken);
      vFrame.setEncryptedTokenCookie(result.encryptedAccessToken);
      location.search = '';
      callback();
    });
  }

  static linkedAccountAuthenticate(switchAccountToken, tenantId, callback) {
    abp.multiTenancy.setTenantIdCookie(tenantId);
    const cookieLangValue = abp.utils.getCookieValue('Abp.Localization.CultureName');

    return abp.ajax({
      url: vFrame.remoteServiceBaseUrl + '/api/TokenAuth/LinkedAccountAuthenticate?switchAccountToken=' + switchAccountToken,
      method: 'POST',
      headers: {
        '.AspNetCore.Culture': ('c=' + cookieLangValue + '|uic=' + cookieLangValue),
        'Abp.TenantId': abp.multiTenancy.getTenantIdCookie()
      }
    }).done(result => {
      abp.auth.setToken(result.accessToken);
      vFrame.setEncryptedTokenCookie(result.encryptedAccessToken);
      location.search = '';
      callback();
    });
  }

  static getUserConfiguration(callback) {
    const cookieLangValue = abp.utils.getCookieValue('Abp.Localization.CultureName');
    return abp.ajax({
      url: vFrame.remoteServiceBaseUrl + vFrame.remoteStaticApi.getAll,
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + abp.auth.getToken(),
        '.AspNetCore.Culture': ('c=' + cookieLangValue + '|uic=' + cookieLangValue),
        'Abp.TenantId': abp.multiTenancy.getTenantIdCookie(),
        'Accept-Language':cookieLangValue
      }
    }).done(result => {
      $.extend(true, abp, result);

      abp.clock.provider = ClockHelper.getCurrentClockProvider(result.clock.provider);
      moment.locale(abp.localization.currentLanguage.name);
      window.moment.locale(abp.localization.currentLanguage.name);

      if (abp.clock.provider.supportsMultipleTimezone) {
        moment.tz.setDefault(abp.timing.timeZoneInfo.iana.timeZoneId);
        window.moment.tz.setDefault(abp.timing.timeZoneInfo.iana.timeZoneId);
      }

      abp.event.trigger('abp.dynamicScriptsInitialized');

      vFrame.recaptchaSiteKey = abp.setting.get('Recaptcha.SiteKey');
      vFrame.subscriptionExpireNootifyDayCount = parseInt(abp.setting.get('App.TenantManagement.SubscriptionExpireNotifyDayCount'));

      //INFO:加载rtl 多语言js  （暂时无必要）
      // LocalizedResourcesHelper.loadResources(callback);
      callback();
    });
  }
}
