/**
 * Created by M on 2017/11/14.
 */
import vFrame from '../core.js'

/*
 用于路由判断（依赖ABP）
 router.beforeEach((to, from, next) => { ..canActivate.. })
 */
class RouterGuard {
  canActivate(to, from, next, session) {
    if (to.matched.filter(t => t.path == '/account').length > 0) {
      next();
    } else if (!session.user) {
      next({
        name: 'login'
      });
    } else if (!to.meta.permissionName) {
      next();
    } else if (vFrame.isGranted(to.meta.permissionName)) {
      next();
    } else {
      next(this.selectBestRoute(session));
    }
  }

  selectBestRoute(session) {
    if (!session.user) {
      return '/account/login';
    }
    if (vFrame.isGranted('Pages.Administration.Dashboard')) {
      return '/dashboard';
    }
    if (vFrame.isGranted('Pages.Tenants')) {
      return '/tenants';
    }
    if (vFrame.isGranted('Pages.Administration.Users')) {
      return '/users';
    }
    return '/';
  }
}

export default new RouterGuard()
