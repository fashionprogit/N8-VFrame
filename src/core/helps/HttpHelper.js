/**
 * Created by M on 2017/11/3.
 */
import axios from "axios";
import vFrame from "../core.js";

/**
 * 参考AbpHttpConfiguration(abpHttp.ts)
 */
class HttpConfiguration {
  constructor() {
    this.defaultError = {
      message: "An error has occurred!",
      details: "Error detail not sent by server."
    };
    this.defaultError401 = {
      message: "You are not authenticated!",
      details:
        "You should be authenticated (sign in) in order to perform this operation."
    };
    this.defaultError403 = {
      message: "You are not authorized!",
      details: "You are not allowed to perform this operation."
    };
    this.defaultError404 = {
      message: "Resource not found!",
      details: "The resource requested could not found on the server."
    };
  }

  logError(error) {
    abp.log.error(error);
  }

  showError(error) {
    if (error.details) {
      return abp.message.error(
        error.details,
        error.message || this.defaultError.message
      );
    } else {
      return abp.message.error(error.message || this.defaultError.message);
    }
  }

  handleTargetUrl(targetUrl) {
    if (!targetUrl) {
      location.href = vFrame.appBaseUrl + "/#/account/login"; //TODO:通过href导航? vue-router是否能监听
    } else {
      location.href = targetUrl;
    }
  }

  handleUnAuthorizedRequest(messagePromise, targetUrl) {
    const self = this;
    if (messagePromise) {
      messagePromise.done(() => {
        this.handleTargetUrl(targetUrl);
      });
    } else {
      self.handleTargetUrl(targetUrl);
    }
  }

  handleNonAbpErrorResponse(response) {
    const self = this;

    switch (response.status) {
      case 401:
        self.handleUnAuthorizedRequest(
          self.showError(self.defaultError401),
          "/"
        );
        break;
      case 403:
        self.showError(self.defaultError403);
        break;
      case 404:
        self.showError(self.defaultError404);
        break;
      default:
        self.showError(self.defaultError);
        break;
    }
  }

  handleAbpResponse(response, ajaxResponse) {
    var newResponse = {
      url: response.url,
      body: ajaxResponse,
      headers: response.headers,
      status: response.status,
      statusText: response.statusText,
      type: response.type
    };

    if (ajaxResponse.success) {
      newResponse.body = ajaxResponse.result;

      if (ajaxResponse.targetUrl) {
        this.handleTargetUrl(ajaxResponse.targetUrl);
      }
    } else {
      if (!ajaxResponse.error) {
        ajaxResponse.error = this.defaultError;
      }

      this.logError(ajaxResponse.error);
      this.showError(ajaxResponse.error);

      if (response.status === 401) {
        this.handleUnAuthorizedRequest(null, ajaxResponse.targetUrl);
      }
    }

    return Object.assign({}, newResponse);
  }

  getAbpAjaxResponseOrNull(response) {
    if (!response || !response.headers) {
      return null;
    }

    var contentType = response.headers["content-type"];
    if (!contentType) {
      abp.log.warn("Content-Type is not sent!");
      return null;
    }

    if (contentType.indexOf("application/json") < 0) {
      abp.log.warn("Content-Type is not application/json: " + contentType);
      return null;
    }

    var responseObj = response.data;
    if (!responseObj.__abp) {
      return null;
    }

    return responseObj;
  }

  handleResponse(response) {
    var ajaxResponse = this.getAbpAjaxResponseOrNull(response);
    if (ajaxResponse == null) {
      return response;
    }

    return this.handleAbpResponse(response, ajaxResponse);
  }

  handleError(error) {
    var ajaxResponse = this.getAbpAjaxResponseOrNull(error);
    if (ajaxResponse != null) {
      this.handleAbpResponse(error, ajaxResponse);
      throw ajaxResponse.error;
    } else {
      this.handleNonAbpErrorResponse(error);
      throw "HTTP error: " + error.status + ", " + error.statusText;
    }
  }
}

/**
 * http请求帮助类
 */
export default class HttpHelper {
  constructor() {
    this.httpConfigration = new HttpConfiguration();
  }

  request(url, body, options) {
    if (!options) {
      options = {};
    }
    this.normalizeRequestOptions(options);

    if (!body) {
      options.mehods = "GET";
    } else {
      options.method = "POST";
      options.data = body;
    }

    //用于兼容前后台部署在同一个网站下
    if (!vFrame.remoteServiceBaseUrl) {
      if (url.indexOf("/") == 0) {
        url = url.substring(1);
      }
    }

    options.url = vFrame.remoteServiceBaseUrl + url;

    return axios(options)
      .then(response => this.httpConfigration.handleResponse(response))
      .catch(error => this.httpConfigration.handleError(error.response))
      .then(response => response.body);
  }

  normalizeRequestOptions(options) {
    if (!options.headers) {
      options.headers = {};
    }

    options.headers["Pragma"] = "no-cache";
    options.headers["Cache-Control"] = "no-cache";
    options.headers["Expires"] = "Sat, 01 Jan 2000 00:00:00 GMT";

    this.addXRequestedWithHeader(options);
    this.addAuthorizationHeaders(options);
    this.addAspNetCoreCultureHeader(options);
    this.addAcceptLanguageHeader(options);
    this.addTenantIdHeader(options);
  }

  addXRequestedWithHeader(options) {
    if (options.headers) {
      options.headers["X-Requested-With"] = "XMLHttpRequest";
    }
  }

  addAspNetCoreCultureHeader(options) {
    let cookieLangValue = abp.utils.getCookieValue(
      "Abp.Localization.CultureName"
    );
    if (
      cookieLangValue &&
      options.headers &&
      !options.headers[".AspNetCore.Culture"]
    ) {
      options.headers[".AspNetCore.Culture"] = cookieLangValue;
    }
  }

  addAcceptLanguageHeader(options) {
    let cookieLangValue = abp.utils.getCookieValue(
      "Abp.Localization.CultureName"
    );
    if (
      cookieLangValue &&
      options.headers &&
      !options.headers["Accept-Language"]
    ) {
      options.headers["Accept-Language"] = cookieLangValue;
    }
  }

  addTenantIdHeader(options) {
    let cookieTenantIdValue = abp.utils.getCookieValue("Abp.TenantId");
    if (
      cookieTenantIdValue &&
      options.headers &&
      !options.headers["Abp.TenantId"]
    ) {
      options.headers["Abp.TenantId"] = cookieTenantIdValue;
    }
  }

  addAuthorizationHeaders(options) {
    let authorizationHeaders = options.headers
      ? options.headers["Authorization"]
      : null;
    if (!authorizationHeaders) {
      authorizationHeaders = [];
    }

    if (
      !this.itemExists(
        authorizationHeaders,
        item => item.indexOf("Bearer ") == 0
      )
    ) {
      let token = abp.auth.getToken();
      if (options.headers && token) {
        options.headers["Authorization"] = "Bearer " + token;
      }
    }
  }

  itemExists(items, predicate) {
    for (let i = 0; i < items.length; i++) {
      if (predicate(items[i])) {
        return true;
      }
    }
    return false;
  }
}
