import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import coreEX from '@/components/core/coreEX'
import httpEX from '@/components/http/httpEX'
import abpEX from '@/components/abp/abpEX'
import nuploadEX from '@/components/upload/nuploadEX'
import nmutipleuploadEX from '@/components/upload/nmutipleuploadEX'
import toolbarEX from '@/components/toolbar/nToolbarEX'
import layoutEX from '@/components/layout/nLayoutEX'
import nTableEX from '@/components/dataTable/nTableEX'
import nDatepickerEX from '@/components/datepicker/nDatepickerEX'
import nDialogEX from '@/components/dialog/nDialogEX'
import nLookpuEX from '@/components/lookup/nLookupEX'
import chatBarEX from '@/components/chat/chatBarEX'
import advanceFilterPanelEX from '@/components/advanceFilterPanel/advanceFilterPanelEX'
import nEditorEx from '@/components/editor/nEditorEx'
import nQrEx from '@/components/qrcode/nQrEx'
import reportDesignerEx from '@/components/report/reportDesignerEx'
import reportViewerEx from '@/components/report/reportViewerEx'
import keycontent from '@/components/keyboard/keyContent.vue'
Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'home',
    component: home,
    title: "VFrame 说明",
    children: [{
      path: "/core",
      name: "core",
      component: coreEX,
      title: "核心VFrame"
    },
      {
        path: "/abp",
        name: "abp",
        component: abpEX,
        title: "核心ABP"
      },
      {
        path: "/http",
        name: "http",
        component: httpEX,
        title: "核心ABP.HTTP"
      },
      {
        path: "/upload",
        name: "upload",
        component: nuploadEX,
        title: "单文件上传"
      },
      {
        path: "/mutipleUpload",
        name: "mutipleUpload",
        component: nmutipleuploadEX,
        title: "多文件上传"
      },
      {
        path: "/toolbar",
        name: "toolbar",
        component: toolbarEX,
        title: "动作条"
      },
      {
        path: "/layout",
        name: "layout",
        component: layoutEX,
        title: "布局"
      },
      {
        path: "/table",
        name: "table",
        component: nTableEX,
        title: "数据表格"
      },
      {
        path: "/datepicker",
        name: "datepicker",
        component: nDatepickerEX,
        title: "日期选择"
      },
      {
        path: "/dialog",
        name: "dialog",
        component: nDialogEX,
        title: "弹出窗口"
      },
      {
        path: "/nLookpuEX",
        name: "nLookpuEX",
        component: nLookpuEX,
        title: "Lookup"
      },
      {
        path: "/chatBarEX",
        name: "chatBarEX",
        component: chatBarEX,
        title: "聊天窗口"
      },
      {
        path: "/advanceFilterPanelEX",
        name: "advanceFilterPanelEX",
        component: advanceFilterPanelEX,
        title: "高级查询显示隐藏"
      },
      {
        path: "/nEditorEx",
        name: "nEditorEx",
        component: nEditorEx,
        title: "编辑器"
      },
      {
        path: "/nQrEx",
        name: "nQrEx",
        component: nQrEx,
        title: "二维码"
      },
      {
        path: "/reportDesignerEx",
        name: "reportDesignerEx",
        component: reportDesignerEx,
        title: "报表设计"
      },
      {
        path: "/reportViewerEx",
        name: "reportViewerEx",
        component: reportViewerEx,
        title: "报表展示"
      },
      {
        path:"/keycontent",
        name:"keycontent",
        component:keycontent,
        title:"虚拟键盘"
      }
    ]
  }],
})
